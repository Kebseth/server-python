from flask import Flask, request  # pip install flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "PIOU PIOU"

@app.route('/', methods=['POST'])
def parse_request():
    display_data = request.get_data()
    data = request.data
    filename = "data.txt"
    myfile = open(filename , "w")
    myfile.write(data.decode("UTF-8") + "\n")
    myfile.close()
    return display_data
	
if __name__ == "__main__":
    app.run(host= '0.0.0.0')